import { useState, useEffect } from "react";
import { Tezos } from "@taquito/taquito";

const CONTRACT_ADDRESS = "KT1DiSo2Y9Upy12fBEo5kHQa3g3YzUK6wKUs";

export function useContract() {
  const [contract, setContract] = useState(null);
  const [error, setError] = useState("");
  const [storage, setStorage] = useState(null);
  const [loading, setLoading] = useState(false);
  const [operationsCount, setOperationsCounter] = useState(0);

  useEffect(() => {
    loadStorage(contract);
  }, [contract, operationsCount]);

  return {
    contract,
    error,
    storage,
    loading,
    operationsCount,
    loadStorage,
    connect,
    increaseOperationsCount,
  };

  function increaseOperationsCount() {
    setOperationsCounter(operationsCount + 1);
  }

  async function connect() {
    setLoading(true);
    try {
      const contractInstance = await Tezos.wallet.at(CONTRACT_ADDRESS);
      setContract(contractInstance);
      increaseOperationsCount();
    } catch (err) {
      setError(err.message);
    } finally {
      setLoading(false);
    }
  }
  async function loadStorage(contract) {
    if (!contract) {
      return;
    }
    try {
      setLoading(true);
      const storage = await contract.storage();
      setStorage(storage);
      //await setLoading(false);
    } catch (e) {
      setError(e.message);
    } finally {
      await setLoading(false);
    }
  }
}
