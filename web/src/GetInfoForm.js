import React from "react";

export function GetInfoForm({ getName }) {
  const [id, setId] = React.useState("");
  const [name, setName] = React.useState("");

  return (
    <p>
      Enter ID:
      <input value={id} onChange={(e) => setId(e.target.value)}></input>
      <button onClick={onSubmit}>Get Name</button>
      <span> Name: {name}</span>
    </p>
  );

  function onSubmit() {
    const name = getName(id); //sets name to the id of getName
    setName(name); //sets getName as the name
  }
}
