import React from "react";

export function SetInfoForm({ addApplicant, newId, loading }) {
  //console.log(newId);
  const [name, setName] = React.useState("");
  return (
    <p>
      Enter Name:
      <input value={name} onChange={(e) => setName(e.target.value)}></input>
      <button
        name={"Button"}
        onClick={handleSubmit}
        disabled={loading === true}
      >
        Input Name to Map
      </button>
      <span> Next Id: {newId}</span>
    </p>
  );
  function handleSubmit() {
    addApplicant(name);
    setName("");

    //need to make a loading state in App.js
    //when add applicant is called - set loading = true, when finished, set loading = false, if loading = true, show spinner(or string that says loading) and hide button

    ///const gsize = getSize(); //sets name to the id of getName
    ///setSize(name);
  }
}
