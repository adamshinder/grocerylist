import React, { useEffect } from "react";

import { useWallet } from "./hooks/use-wallet";
import { useBalanceState } from "./hooks/use-balance-state";
import { useContract } from "./hooks/use-contract";

import { GetInfoForm } from "./GetInfoForm";
import { SetInfoForm } from "./SetInfoForm";

export default function App() {
  const {
    initialized,
    address,
    error: walletError,
    loading: walletLoading,
    connect: connectToWallet,
  } = useWallet();

  const {
    storage, //MichelsonMap because ther is only one map in storage, storage = map
    error: contractError,
    loading: contractLoading,
    contract,
    operationsCount,
    loadStorage,
    connect: connectToContract,
  } = useContract();
  const {
    balance,
    error: balanceError,
    loading: balanceLoading,
  } = useBalanceState(address, operationsCount);

  const [operationError, setOperationError] = React.useState("");
  const [loading, setLoading] = React.useState(false);

  //  if loading = true, show spinner(or string that says loading) and hide button

  const [newId, setnewId] = React.useState(0); //0 is the default because storage isn't a variable in the first render
  useEffect(() => {
    if (storage) {
      setnewId(storage.size + 1);
    }
  }, [storage]);

  useEffect(() => {
    console.log("Loading State Changed!");
  }, [loading]); // on a loading change, do something

  console.log(storage);

  return (
    <div className="app">
      {initialized && (
        <>
          <div>Address: {walletLoading ? "Loading..." : address}</div>
          <div>Balance: {balanceLoading ? "Loading..." : balance}</div>
        </>
      )}
      {walletError && <div>Wallet Error: {walletError}</div>}
      {balanceError && <div>Balance Error: {balanceError}</div>}
      {contractError && <div>Contract Error: {contractError}</div>}
      {operationError && <div>Operation Error: {operationError}</div>}
      {initialized ? (
        <>
          <GetInfoForm getName={getName} />
          <p>Creates a new candidate:</p>
          <SetInfoForm
            addApplicant={addApplicant}
            newId={newId}
            loading={loading}
          />
        </>
      ) : (
        <button onClick={connect}> Connect </button>
      )}
    </div>
  );
  async function connect() {
    await connectToWallet();
    await connectToContract();
  }
  function getName(id) {
    //pulls the information
    if (storage) {
      const name = storage.get(id);
      return name || "Id doesn't exist";
    }
  }

  async function addApplicant(name) {
    if (storage) {
      try {
        await setLoading(true);
        const setName = await contract.methods
          .main(Number(newId), String(name))
          .send();
        await setName.confirmation();
        await loadStorage(contract); // this sets loading === true because of the function in use-contract
      } catch (error) {
        setOperationError(error.message);
      } finally {
        await setLoading(false);
      }
    }
  }
}
