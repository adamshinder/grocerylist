//import {MichelsonMap} from '@taquito/taquito';
const { MichelsonMap } = require("@taquito/taquito");
const College = artifacts.require("Grocery");

module.exports = (deployer) => {
  deployer.deploy(College, new MichelsonMap());
}; //deployer.deploy(College, {names: new MichelsonMap(19, "Adam") });
