type item = {
name: string,
amount: nat
};

type storage = {
  groceries: map (int, item) 
};

type action =
| CreateItem ((int, item));

type returnType = (list(operation), storage);

let createItem = ((s, info): (storage, (int, item))): storage => {
  switch (Map.find_opt(info[0], s.groceries)) {
    | None => {...s, groceries: Map.add(info[0], info[1], s.groceries)}
    | Some (acc) =>  {...s, groceries: Map.update(info[0], Some (info[1]), s.groceries)}
  };
};

//let removeItem

let main = ((p,storage): (action, storage)) => {
  let storage =
    switch (p) {
    | CreateItem (n) => createItem((storage, n))
    };
  ([]: list(operation), storage);
};

